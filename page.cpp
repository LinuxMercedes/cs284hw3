#include<iostream>
#include<fstream>
#include<cstring>
#include<cstdlib>

#define WORD_CNT 512
#define PAGE_CNT(sz) (WORD_CNT/(sz))
#define WORD2PAGE(w, sz) ((w)/(sz))
#define WORD2PAGE_CNT(w, sz) (((w)%(sz))?((w)/(sz)) + 1:(w)/(sz))
#define PAGE(x, sz) ((x)/PAGE_CNT(sz))
#define INITIAL_LOAD(cnt, sz) (PAGE_CNT(sz)/(cnt))

using namespace std;

enum replacement_alg {
  CLOCK,
  LRU,
  FIFO
};

struct Page {
  unsigned int frame; /* Frame number */
  bool allocd;        /* Is alloc'd or not */
};

struct Frame {
  Page* page;        /* Page reference */
  bool used;         /* Usage bit */
  unsigned long ts;  /* Timestamp */
};

struct PageTable {
  Page* pages;       /* Pages */
  unsigned int size; /* Number of pages */
};

struct State {
  Frame** main;                          /* main memory */
  PageTable* pgtable;                    /* Page table */
  unsigned int hand;                     /* Clock replacement pointer */
  unsigned long clock;                   /* Modification/Insert clock */
  unsigned int pagesize;                 /* Page Size */
  unsigned long faults;                  /* Page fault count*/
  bool prepage;                          /* Prepage/demand page */
  replacement_alg ralg;                  /* Page replacement alg */
  void (*place)(State& state, Page* pg); /* Page replacement fcn */
};

struct Program {
  unsigned int number;  /* Which program is this */
  unsigned int alloc;   /* Number of words allocated */

  Program* next;        /* We're making a linked list, bitches! */
};

unsigned int findoldest(State& state, Page* pg) {
  unsigned long maxdiff = 0;
  unsigned int idx = 0;

  for(unsigned int i = 0; i < PAGE_CNT(state.pagesize); ++i) {
    unsigned long diff = state.clock - state.main[i]->ts;
    if(diff > maxdiff) {
      maxdiff = diff;
      idx = i;
    }
  }

  state.main[idx]->page->allocd = false;
  state.main[idx]->page = pg;
  state.main[idx]->ts = state.clock;

  return idx;
}

void lru(State& state, Page* pg) {
  pg->frame = findoldest(state, pg);
  pg->allocd = true;
}

void fifo(State& state, Page* pg) {
  pg->frame = findoldest(state, pg);
  pg->allocd = true;
}

void clock(State& state, Page* pg) {
  while(state.main[state.hand]->used) {
    state.main[state.hand]->used = false;
    state.hand = (state.hand + 1)%PAGE_CNT(state.pagesize); // Prevent overflow
  }

  state.main[state.hand]->used = true;
  state.main[state.hand]->page->allocd = false;
  state.main[state.hand]->page = pg;
  pg->frame = state.hand;
  pg->allocd = true;
}

void getPage(State& state, unsigned int prog, unsigned int addr) {
  state.clock++;

  Page& pg = state.pgtable[prog].pages[WORD2PAGE(addr, state.pagesize)];
  if(!pg.allocd) { /* Page Fault: Swap a new page (or two) in */
    state.faults++;
    state.place(state, &pg);

    if(state.prepage) {
      addr = (addr + 1)%state.pgtable[prog].size; // Prevent overflow
      Page& nxtpg = state.pgtable[prog].pages[WORD2PAGE(addr,state.pagesize)];
      state.place(state, &nxtpg);
    }
  }
  else { /* Page already in memory: update access bits */
    switch(state.ralg) {
      case CLOCK:
        state.main[pg.frame]->used = true;
        break;
      case LRU:
        state.main[pg.frame]->ts = state.clock;
        break;
    }
  }
}

void run(char* list, char* trace, int pagesize, replacement_alg ralg, bool prepage) {
  cout << "Preparing state..." << endl;
  Program* proglist;
  Program* plptr;
  Program* lastprog;
  unsigned int progCount = 0;
  unsigned int pgCount = 0;

  State state;
  state.pagesize = pagesize;
  state.clock = 0;
  state.hand = 0;
  state.faults = 0;

  state.prepage = prepage;

  state.ralg = ralg;
  switch(ralg) {
    case CLOCK:
      state.place = &clock;
      break;
    case LRU:
      state.place = &lru;
      break;
    case FIFO:
      state.place = &fifo;
      break;
  }

  cout << "Reading programs..." << endl;
  // Read in the list of programs and required allocations 
  ifstream listIn(list);

  proglist = new Program;
  plptr = proglist;

  while(listIn >> plptr->number) {
    listIn >> plptr->alloc;
    pgCount += plptr->alloc;
    progCount++;
    lastprog = plptr;
    plptr->next = new Program;
    plptr = plptr->next;
  }

  listIn.close();

  // The linked list stuff allocates one too many programs
  delete lastprog->next;
  lastprog->next = NULL;

  cout << "Preparing page tables..." << endl;
  // Set up state arrays
  state.main = new Frame*[PAGE_CNT(state.pagesize)];
  state.pgtable = new PageTable[progCount];

  // Make each page table
  plptr = proglist;
  unsigned int frameptr = 0;
  while(plptr) {
    state.pgtable[plptr->number].size = WORD2PAGE_CNT(plptr->alloc, state.pagesize);
    state.pgtable[plptr->number].pages = new Page[state.pgtable[plptr->number].size];

    // Fill the page table with default pages
    for(unsigned int i = 0; i < state.pgtable[plptr->number].size; ++i) {
      state.pgtable[plptr->number].pages[i].allocd = false;
      state.pgtable[plptr->number].pages[i].frame = 0;
    }

    // Fill main memory with the appropriate load
    for(unsigned int i = 0; i < INITIAL_LOAD(progCount, pagesize); ++i) {
      Page& pg = state.pgtable[plptr->number].pages[i];
      pg.frame = frameptr;
      pg.allocd = true;
      state.main[frameptr] = new Frame;
      state.main[frameptr]->page = &pg;
      state.main[frameptr]->used = false;
      state.main[frameptr]->ts = state.clock;
      frameptr++;
    }

    // Fill the last bits of main memory 
    if((plptr->next == NULL) && frameptr < PAGE_CNT(pagesize)) {
      unsigned int i = INITIAL_LOAD(progCount, pagesize);
      while(frameptr < PAGE_CNT(pagesize)) {
        Page& pg = state.pgtable[plptr->number].pages[i];
        pg.frame = frameptr;
        pg.allocd = true;
        state.main[frameptr] = new Frame;
        state.main[frameptr]->page = &pg;
        state.main[frameptr]->used = false;
        state.main[frameptr]->ts = state.clock;
        frameptr++;
        i++;
      }
    }

    plptr = plptr->next;
  }

  cout << "Running trace..." << endl;
  // Start handling memory requests
  ifstream trIn(trace);
  unsigned int prog;
  unsigned int addr;
  while(trIn >> prog) {
    trIn >> addr;

    getPage(state, prog, addr);
  }
  trIn.close();

  cout << "Cleaing up..." << endl;
  plptr = proglist;
  while(plptr) {
    delete [] state.pgtable[plptr->number].pages;
    Program* tmp = plptr;
    plptr = plptr->next;
    delete tmp;
  }

  for(unsigned int i = 0; i < PAGE_CNT(state.pagesize); ++i) {
    delete state.main[i];
  }
  delete [] state.main;

  delete [] state.pgtable;

  cout << "Page faults: " << state.faults << endl;
}

void usage(char* progname) {
  cout << "Usage: " << progname << " <program list> <trace file> <page size> <clock | lru | fifo> <1 | 0> (prepaging/demand paging)" << endl;
}

int main(int argc, char** argv) {
  if(argc == 1) { /* Nothing provided; print usage and quit */
    usage(argv[0]);
    exit(0);
  }

  if(argc != 6) { /* Need more args */
    cerr << "Invalid number of parameters" << endl;
    usage(argv[0]);
    exit(-1);
  }

  char* list = argv[1];
  char* trace = argv[2];

  int pagesize = atoi(argv[3]);
  if(pagesize <= 0) { /* Invalid pagesize */
    cerr << "Invalid page size " << argv[3] << endl;
    usage(argv[0]);
    exit(-2);
  }

  char* rep = argv[4];
  replacement_alg ra;
  if(!strcmp("clock", rep)) {
    ra = CLOCK;
  }
  else if(!strcmp("lru", rep)) {
    ra = LRU;
  }
  else if(!strcmp("fifo", rep)) {
    ra = FIFO;
  }
  else { /* Invalid page replacement */
    cerr << "Invalid page replacement algorithm " << rep << endl;
    usage(argv[0]);
    exit(-2);
  }

  bool prepaging = strcmp(argv[5], "0");

  run(list, trace, pagesize, ra, prepaging);

  exit(0);
}
